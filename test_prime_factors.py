from prime_factors import generate_prime_factors
from unittest import TestCase


class TestPrimeFactors(TestCase):

    def test_one_should_give_empty_list(self):
        self.assertEqual(generate_prime_factors(1), [])

    def test_two_should_give_two(self):
        self.assertEqual(generate_prime_factors(2), [2])

    def test_three_should_give_three(self):
        self.assertEqual(generate_prime_factors(3), [3])

    def test_four_should_give_two_two(self):
        self.assertEqual(generate_prime_factors(4), [2, 2])

    def test_six_should_give_two_three(self):
        self.assertEqual(generate_prime_factors(6), [2, 3])

    def test_eight_should_give_two_two_two(self):
        self.assertEqual(generate_prime_factors(8), [2, 2, 2])

    def test_nine_should_give_three_three(self):
        self.assertEqual(generate_prime_factors(9), [3, 3])

    def test_ten_should_give_two_five(self):
        self.assertEqual(generate_prime_factors(10), [2, 5])