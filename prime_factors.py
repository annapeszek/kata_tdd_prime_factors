from typing import List


def generate_prime_factors(number: int) -> List[int]:
    prime_factor_list = []
    candidate = 2
    while not number == 1:
        while not number % candidate:
            prime_factor_list.append(candidate)
            number = number / candidate
        candidate += 1
    return prime_factor_list